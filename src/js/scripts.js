$(document).ready(function () {
    index();
    $('#pagination').hide();
    $( "#search_buttom" ).bind( "click", function() {
        var tosearch = $("#search_imput" ).val();
        if(tosearch != ''){
            search(tosearch);
        }
    });
});

function index() {
    var data='';
    var imagenes=''
    var imagenes_=''
    var url = "https://api.nytimes.com/svc/archive/v1/2016/2.json";
    url += '?' + $.param({
            'api-key': "284f3be0f18a424db592dfdc0b6208f4"
        });
    $.ajax({
        url: url,
        method: 'GET',
    }).done(function (result){
        var count = 0;
        result= result.response.docs;
        $.each(result, function (index) {
            var result_ ={};
            result_= result[index];
            if(index < 3){
                var dateVar = result_.pub_date;
                var d = new Date(dateVar);
                var day = formattedDate(d);
                var time = day [0]+ "/" + day [1] + "-" +day [2];
                var img;
                if(result_.multimedia.length == 0 || result_.lead_paragraph ==null || result_.news_desk ==null){return;}
                img = 'http://www.nytimes.com/'+result_.multimedia[1].url;
                data +=  "<div class='article col-md-12'>"+
                    "<div class='col-md-4'><img src='"+img+"' class='img-responsive a_img img-thumbnail'alt='Responsive image'></div>"+
                    "<div class='col-md-8 art_gent'>"+
                    "<div class='col-md-12 a_title'>"+result_.news_desk+"</div>"+
                    "<div class='col-md-12 a_date'>Posted: "+time+"</div>"+
                    "<div class='col-md-12 a_content'>"+
                    "<p>"+result_.lead_paragraph+"</p>"+
                    "<a class='btn btn-primary btn_art'  target='_blank' href="+result_.web_url+">Read more</a>"+
                    "</div></div></div>";
            }
            else{
                return false
            }
        });
        $.each(result, function (index) {
            var result_ ={};
            result_= result[index];
            if(count < 18){
                if(result_.multimedia.length == 0){return;}
                img = 'http://www.nytimes.com/'+result_.multimedia[2].url;
                imagenes +=  "<div class='col-xs-4 col-md-2'><img src='"+img+"'class='img-responsive a_img img-thumbnail' alt='"+result_.section_name+"'></div>";
                if(count > 8){
                    imagenes_ += "<div class='col-xs-4'><img src='"+img+"'class='img-responsive a_img img-thumbnail' alt='"+result_.section_name+"'></div>";
                }
                count++;
            }
            else{
                return false
            }
        });
        $('#articles').html(data);
        $('#ima_spons').html(imagenes);
        $('#flickr').html(imagenes_);
        $('#pagination').show();
    }).fail(function (err) {
        throw err;
    });
}
function search(tosearch) {
    var data='';
    var url = "https://api.nytimes.com/svc/search/v2/articlesearch.json";
    url += '?' + $.param({
            'api-key': "284f3be0f18a424db592dfdc0b6208f4",
            'q': tosearch,
            'sort': "newest"
        });
    $.ajax({
        url: url,
        method: 'GET',
    }).done(function(result) {
        $('#sponsoret').hide();
        $('#pagination').hide();
        var data_result= {};
        if(result.response.docs.length >0){
            data = '<h1 class="navbar-inverse"> You Results ...<h1>';
            data_result = result.response.docs;
            $.each(data_result, function (index) {
            var result_ ={};
            result_= data_result[index];
            if(index < 125){
                if(result_.multimedia.length == 0 || result_.lead_paragraph ==null || result_.news_desk ==null  || result_.pub_date ==null){return;}
                var dateVar = result_.pub_date;
                var d = new Date(dateVar);
                var day = formattedDate(d);
                var time = day [0]+ "/" + day [1] + "-" +day [2];
                var img;
                img = 'http://www.nytimes.com/'+result_.multimedia[1].url;
                data +=  "<div class='article col-md-12'>"+
                    "<div class='col-md-4'><img src='"+img+"' class='img-responsive a_img img-thumbnail'alt='Responsive image'></div>"+
                    "<div class='col-md-8 art_gent'>"+
                    "<div class='col-md-12 a_title'>"+result_.news_desk+"</div>"+
                    "<div class='col-md-12 a_date'>Posted: "+time+"</div>"+
                    "<div class='col-md-12 a_content'>"+
                    "<p>"+result_.lead_paragraph+"</p>"+
                    "<a class='btn btn-primary btn_art'  target='_blank' href="+result_.web_url+">Read more</a>"+
                    "</div></div></div>";
            }
            else{
                return false
            }
        })
        }
        else{
            data = '<h1 class="navbar-inverse"> Dont have any results ...<h1>';
        }

        $('#articles').html(data);
    }).fail(function(err) {
        throw err;
    });
}
function formattedDate(date) {
    var d = new Date(date || Date.now()),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [month, day, year];
}
(function($){
    "use strict";
    $('a.page-scroll').bind('click', function(event) {
        var $anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: ($($anchor.attr('href')).offset().top - 50)}, 1000, 'easeInOutExpo');
        event.preventDefault();
    });
    new WOW().init();
})(jQuery);